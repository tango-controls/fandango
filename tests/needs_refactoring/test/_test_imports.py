#!/usr/bin/env python

#############################################################################
##
## project :     Tango Control System
##
## $Author: Sergi Rubio Manrique, srubio@cells.es $
##
## $Revision: 2008 $
##
## copyleft :    ALBA Synchrotron Controls Section, CELLS
##               Bellaterra
##               Spain
##
#############################################################################
##
## This file is part of Tango Control System
##
## Tango Control System is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as published
## by the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## Tango Control System is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, see <http://www.gnu.org/licenses/>.
###########################################################################

from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from past.utils import old_div
import pytest

pytest.skip("Those tests need refactoring", allow_module_level=True)

import traceback
import sys


def test_fandango_imports():
    import fandango

    assert fandango
    assert fandango.objects
    assert fandango.functional
    assert fandango.dicts
    assert fandango.threads
    import fandango.db

    assert fandango.db

    try:
        import PyTango

        assert fandango.tango
        assert fandango.servers
        assert fandango.dynamic
        assert fandango.device
        assert fandango.device.DevChild
        assert fandango.device.DDebugClass
    except:
        pass

    try:
        from PyQt4 import Qt
        import fandango.qt

        assert fandango.qt
    except:
        pass

    import fandango.functional as fun


def test_fandango_other_imports():
    class Skip(Exception):
        pass

    # Testing fandango imports
    m = "fandango.__all__"
    print()
    "Loading fandango ..."
    import fandango

    print()
    "ReleaseNumber = %s" % str(fandango.RELEASE)
    import fandango.functional as fun

    print("\n\n" + m + ": OK")
    # Testing fandango.$module passed as argument
    a = fun.first(sys.argv[1:], "*")
    try:
        m = "fandango.functional"
        if not fun.searchCl(a, m):
            raise Skip()

        assert fun.searchCl("id24eu & xbpm", "id24eu_XBPM_naaa", extend=True)
        assert not fun.searchCl("id24eu & !xbpm", "id24eu_XBPM_naaa", extend=True)
        assert fun.searchCl("id24eu & !xbpm", "id24eu_PM_naaa", extend=True)
        assert not fun.searchCl("id24eu & xbpm", "id24eu_PM_naaa", extend=True)
        assert None is fun.first([], None)
        assert 1 is fun.first((i for i in range(1, 3)))
        assert 2 is fun.last((i for i in range(1, 3)))
        assert 0 is fun.last([], default=0)
        print(m + ": OK")
    except Skip:
        pass

    try:
        m = "fandango.excepts"
        if not fun.searchCl(a, m):
            raise Skip()
        import fandango.excepts as f_excepts

        assert 1 == f_excepts.trial(
            lambda: old_div(1, 1), lambda e: 10, return_exception=True
        )
        assert 10 == f_excepts.trial(
            lambda: old_div(1, 0), lambda e: 10, return_exception=True
        )
        print(m + ": OK")
    except Skip:
        pass

    try:
        m = "fandango.tango"
        if not fun.searchCl(a, m):
            raise Skip()
        import fandango.tango as f_tango

        assert isinstance(
            f_tango.get_proxy("sys/database/2"), fandango.tango.PyTango.DeviceProxy
        )
        assert isinstance(
            f_tango.get_proxy("sys/database/2/state"),
            fandango.tango.PyTango.AttributeProxy,
        )
        assert isinstance(f_tango.finder(), fandango.tango.PyTango.Database)
        assert isinstance(
            f_tango.finder(f_tango.finder("sys/database/*")[0]),
            fandango.tango.PyTango.DeviceProxy,
        )
        assert isinstance(
            f_tango.finder(f_tango.finder("sys/database/*/st[a]te")[0]), int
        )
        assert f_tango.get_device_info("sys/database/2").dev_class == "DataBase"
        assert fandango.isNaN(
            f_tango.TangoEval(trace=False).eval("da/do/di/du", _raise=fandango.NaN)
        )
        assert fandango.tango.TangoEval(trace=False).eval(
            "sys/database/2", _raise=None
        ) in (0, None)
        print(m + ": OK")
    except Skip:
        pass


if __name__ == "__main__":
    test_fandango_imports()
