the methods to create or move a device from Server A to Server B would be:

in bash:

{noformat}
  tango_servers stop Server/A
  tango_servers stop Server/B
  
  fandango add_new_device Server/B MyClass just/a/device
  fandango put_device_property just/a/device MyProperty "whatever"
  
  tango_servers my.host.name start Server/A
  tango_servers my.host.name start Server/B
  tango_servers my.host.name level Server/B 1
{noformat}


in ipython:

{code:python}
import fandango as fn
fn.Astor('Server/A').stop_servers()
fn.Astor('Server/B').stop_servers()
fn.tango.add_new_device('Server/B','MyClass','just/a/device')
fn.tango.put_device_property('just/a/device','MyProperty','whatever')
fn.Astor('Server/A').start_servers()
fn.Astor('Server/A').start_servers()

{code}

