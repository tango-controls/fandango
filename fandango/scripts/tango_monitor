#!/usr/bin/env python3

__doc__ = """
usage: 
  tango_monitor <device name> [ <attr regexp> ]*
  
  tango_monitor <attr_exp1> <attr_exp2> [ period=3000 ]
"""

import sys
import time
import traceback
import PyTango
import fandango as fn


class MyCallback(object):
    
    counter = 0
    
    def __init__(self):
        self.reset()
        
    def reset(self):
        self.t0 = time.time()
        self.m0 = fn.linos.get_process_memory()
        self.counters = fn.dicts.defaultdict(int)
        self.values = fn.dicts.defaultdict(str)
        self.dups = fn.dicts.defaultdict(int)
        self.ratios = fn.dicts.defaultdict(float)

    def push_event(self,event):
        try:
            MyCallback.counter+=1
            aname = fn.tango.get_normal_name(event.attr_name)
            tt = (time.time()-self.t0)
            self.counters[aname] = self.counters[aname] + 1
            self.ratios[aname] = self.counters[aname] / tt
            value = getattr(event.attr_value,'value',event.attr_value)
            epoch = (fn.ctime2time(event.attr_value.time) 
                     if hasattr(event.attr_value,'time') 
                     else 0)
            quality = getattr(event.attr_value,'quality','NO_Q')
            value = fn.shortstr(value)
            if self.values[aname] == value:
                self.dups[aname] += 1
                
            self.values[aname] = value
            value = str(value)[:40] + '...,%s'%str(quality)
            m1 = fn.linos.get_process_memory()
            print('%s:%s:%s = %s; %s; ct=%d/%d, Hz=%2.2f, dups=%d, leak=%2.1fKbps/%1.3fMb' 
                %  (fn.time2str(epoch or fn.now()),epoch,aname,value,event.event, 
                    self.counters[aname],
                    MyCallback.counter,
                    self.ratios[aname],
                    self.dups[aname],
                    (m1-self.m0)/(1e3*tt),
                    m1/1e6,
                    ))
        except:
            traceback.print_exc()
        

def monitor(*args, **kwargs):
    """
    monitor(device,[attributes])
    
    kwargs: events (event types), period (polling period)
    """
    # Old syntax (device, attrs regexp)
    print(args)
    if (fn.clmatch(fn.tango.retango,args[0]) and args[1:] and
            not any(fn.clmatch(fn.tango.retango,a) for a in args[1:])):
        args = [args[0]+'/'+a for a in args[1:]]

    attrs = fn.join(fn.find_attributes(a) for a in args)
    print('matched %d attributes: %s' % (len(attrs),attrs))
    
    # event filters (deprecated)
    # events = kwargs.get('events',[PyTango.EventType.CHANGE_EVENT])
    events = DEFAULT_EVENTS = ["change",
        #'periodic', 
        'archive', 'quality', 'user_event',
        ]

    eis,subscribed,failed = [],[],[]
    sources = []
    cb = MyCallback() #PyTango.utils.EventCallBack()
    
    models = [(a if '/' in a else d+'/'+a) for a in attrs]
    for a in models:
        (failed,subscribed)[bool(fn.tango.check_attribute_events(a))].append(a)


    fn.EventThread.MinWait = 1e-2 #1e-4
    fn.EventThread.DEFAULT_PERIOD_ms = 300 #0.1        
    
    for m in models:
        try:
            period = kwargs.get('period',(3e3,5*60e3)[m in subscribed])
            if not isinstance(period,(int,float)):
                period = fn.str2time(period)*1e3
                
            print('%s polled every %d ms' % (m,period))
            
            sources.append(fn.EventSource(m,
                enablePolling=bool(period),
                listeners=[cb], 
                use_events=events,
                polling_period=int(period),
                keeptime=1e-2,
                queued=True,
                ))

        except:
            traceback.print_exc()
            m not in failed and failed.append(m)
            m in subscribed and subscribed.remove(m)
    
    fn.EventSource.get_thread().setup(period_ms=50,
                                        latency=5.,filtered=False,)
    fn.EventSource.get_thread().setLogLevel('INFO')

    print('%d attributes NOT provide events: %s' % (len(failed),failed))      
    print('%d attributes provide events: %s' % (len(subscribed),subscribed))
    print('-'*80 + '\n' + '-'*80)
    
    try:
        ct = 0
        while True:
            if ct == 10:
                print('\n\n\n\n\nReset memory counter\n\n\n\n\n')
                cb.reset()
                MyCallback.counter = 0
            time.sleep(1)
            ct+=1
    except: #KeyboardInterrupt
        print(fn.except2str())
        print('-'*80)
        print('Unsubscribing ...')

        for s in sources:
            s.removeListener(cb)
                
        print("Finished monitoring")
        
    #[dp.unsubscribe_event(ei) for ei in eis];
        
if __name__ == '__main__':
    import sys
    try:
        args = sys.argv[1:]
        if not args or args[0] in ('help','--help','-h','-?'):
            raise Exception('No arguments provided!')
        opts = [a for a in args if '=' in a]
        args = [a.strip() for a in args if a not in opts] or ["state"]
        opts = dict((k.strip('-'),fn.str2type(v)) for k,v in (o.split('=',1) 
                                                   for o in opts))
        monitor(*args,**opts)
    except:
        print(fn.except2str())
        print(__doc__)
    

